﻿using System;
using System.Collections.Generic;

namespace LinkedList
{
    public class Node<T>
    {
        public Node<T> next;
        public T data;

        public Node()
        {
        }
        public Node(T _data)
        {
        }
        public Node(Node<T> _next)
        {
        }
        public Node(Node<T> _next, T _data)
        {
            next = _next;
            data = _data;
        }
    }
}

