﻿using System;
using System.Collections.Generic;

namespace LinkedList
{
    public class LinkedList<T>
    {
        public Node<T> head;
        public Node<T> current;
        int size = 0;

        public LinkedList()
        {
            current = head;
        }

        public void Add(T data)
        {
            Node<T> addedElement = new Node<T>();
            addedElement.data = data;
            current.next = addedElement;//generates a nullreferenceexception like if the addedElement is not instantiated...
            current = head;
            size++;
        }

        public int Count()
        {
            return size;
        }
    }
}

